import { Route, Routes } from "react-router-dom";

import Postform from "./components/Postform";
import Postlist from "./components/Postlist";
import { useState } from "react";

function App() {
  const [posts, setPosts] = useState([]);

  const addPost = (post) => {
    setPosts([...posts, post]);
  };

  const editPost = (index, updatedPost) => {
    const updatedPosts = [...posts];
    updatedPosts[index] = updatedPost;
    setPosts(updatedPosts);
  };

  const deletePost = (index) => {
    const updatedPosts = [...posts];
    updatedPosts.splice(index, 1);
    setPosts(updatedPosts);
  };

  return (
    <div className="container">
      <h1>Simple Blog</h1>
      <Routes>
        <Route
          exact
          path="/"
          element={
            <Postlist
              posts={posts}
              editPost={editPost}
              deletePost={deletePost}
            />
          }
        />
        <Route path="/create" element={<Postform addPost={addPost} />} />
        <Route
          path="/edit/:index"
          element={<Postform posts={posts} editPost={editPost} />}
        />
      </Routes>
    </div>
  );
}

export default App;
