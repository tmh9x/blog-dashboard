import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import PropTypes from "prop-types";

const Postform = ({ posts, addPost, editPost }) => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const { index } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    if (index !== undefined && posts && posts[index]) {
      const postData = posts[parseInt(index)];
      setTitle(postData.title);
      setContent(postData.content);
    } else {
      setTitle("");
      setContent("");
    }
  }, [index, posts]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const newPost = { title, content };
    if (index === undefined) {
      addPost(newPost);
    } else {
      editPost(parseInt(index), newPost);
    }
    navigate("/");
  };

  return (
    <div>
      <h2>{index === undefined ? "Create" : "Edit"} Post</h2>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <textarea
          placeholder="Content"
          value={content}
          onChange={(e) => setContent(e.target.value)}
        ></textarea> 
        <button type="submit">
          {index === undefined ? "Create" : "Edit"} Post
        </button>
      </form>
    </div>
  );
};

Postform.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      content: PropTypes.string,
    })
  ),
  addPost: PropTypes.func,
  editPost: PropTypes.func,
  index: PropTypes.oneOfType([PropTypes.number, PropTypes.undefined]),
};

export default Postform;
