import { Link, useNavigate } from "react-router-dom";

import PropTypes from "prop-types";

const Postlist = ({ posts, deletePost }) => {
  const navigate = useNavigate();

  return (
    <div>
      <h2>Blog Posts</h2>
      <ul>
        {posts.map((post, index) => (
          <li key={index}>
            <h3>{post.title}</h3>
            <p>{post.content}</p>
            <button onClick={() => navigate(`/edit/${index}`)}>Edit</button>
            <button onClick={() => deletePost(index)}>Delete</button>
          </li>
        ))}
      </ul>
      <Link to="/create" className="create-post-link">
        Create New Post
      </Link>
    </div>
  );
};

Postlist.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      content: PropTypes.string,
    })
  ).isRequired,
  editPost: PropTypes.func,
  deletePost: PropTypes.func,
};

export default Postlist;
